CREATE DATABASE IF NOT EXISTS `tourist_vouchers`;
USE `tourist_vouchers`;

#################################################################################################


DROP TABLE IF EXISTS `tourist_vouchers_list`;


#################################################################################################

CREATE TABLE `tourist_vouchers_list` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type_tour` VARCHAR(15) NOT NULL,
  `country` VARCHAR(15) NOT NULL,
  `holiday_region` VARCHAR(15) NOT NULL,
  `number_night` INT(11) DEFAULT 1,
  `number_adult` INT(11) DEFAULT 1,
  `price` DECIMAL(10,1) NOT NULL,
  `currency` VARCHAR(15) NOT NULL,
  `hotel_detail` VARCHAR(45) NOT NULL,
  `transport_detail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;;

INSERT  INTO `tourist_vouchers_list`(`id`,  `type_tour`,  `country`,  `holiday_region`,  `number_night`,  `number_adult`,  `price`,  `currency`,  `hotel_detail`,  `transport_detail`) VALUES 
(1,  'beach', 'Greece',  'Rodos', 7,  2,  1500.0,  'BYR',  '3 HB Park view',  'Flight'),
(2,  'shopping', 'China',  'Pekin', 2,  1,  1500.0,  'GBP',  '2 RO Park view',  'Train'),
(3,  'health', 'Belarus',  'Minsk', 1,  2,  1500.0,  'BYR',  '3 HB Park view',  'Bus'),
(4,  'beach', 'Spain',  'Larnaca', 11,  1,  1500.0,  'EUR',  '5 HB Park view',  'Flight'),
(5,  'beach', 'India',  'Goa', 1,  2,  1500.0,  'BYR',  '4 HB Park view',  'Bus'),
(6,  'beach', 'Belarus',  'Minsk', 1,  2,  1500.0,  'BYR',  '3 HB Park view',  'Bus');

#################################################################################################
