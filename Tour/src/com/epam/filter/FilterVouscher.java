package com.epam.filter;

import java.util.ArrayList;
import java.util.List;

import com.epam.model.TouristVoucher;

public class FilterVouscher {
	
	private FilterVouscher() {
	}
	
	public static List<TouristVoucher> filter(List<TouristVoucher> list, String type, String country) {
		List<TouristVoucher> tvAfterParameter = new ArrayList<TouristVoucher>();

		for (TouristVoucher tv : list) {
			if (tv.getType().equalsIgnoreCase(type) && tv.getCountry().equalsIgnoreCase(country)) {
				tvAfterParameter.add(tv);
			}
		}
		return tvAfterParameter;

	}
}
