package com.epam.sort;

import java.util.Comparator;

import com.epam.model.TouristVoucher;

public class PriceSortASC  implements Comparator<TouristVoucher> {

	    @Override
	    public int compare(TouristVoucher o1, TouristVoucher o2) {
	        return o1.getCurrency().compareTo(o2.getCurrency());
	    }
	}
