package com.epam.sort;

import java.util.Comparator;

import com.epam.model.TouristVoucher;

public class TypeHolidaySort implements Comparator<TouristVoucher> {

    @Override
    public int compare(TouristVoucher o1, TouristVoucher o2) {
        return o1.getType().compareTo(o2.getType());
    }

}
