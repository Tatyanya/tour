package com.epam.parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.epam.model.TouristVoucher;

public class XMLParser extends Parser {

	public static final String NAME = "xml";

	private FileInputStream fileAsInputStream;
	private XMLInputFactory inputFactory;

	private TouristVoucher touristVouchersCurrent;

	public XMLParser(String file) {
		try {
			inputFactory = XMLInputFactory.newInstance();
			fileAsInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parse() {

		try {
			XMLStreamReader reader = inputFactory.createXMLStreamReader(fileAsInputStream);
			process(reader);
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	public void process(XMLStreamReader reader) throws XMLStreamException {
		String elementName = null;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				elementName = reader.getLocalName();
				handleStartElement(reader, elementName);
				break;
			case XMLStreamConstants.CHARACTERS:
				handleCharacters(reader, elementName);
				break;
			case XMLStreamConstants.END_ELEMENT:
				handleEndElement(reader, list);
			}

		}
	}

	private void handleEndElement(XMLStreamReader reader, List<TouristVoucher> list) {
		if ("PackageHoliday".equals(reader.getLocalName())) {
			list.add(touristVouchersCurrent);
		}
	}

	private void handleCharacters(XMLStreamReader reader, String elementName) {
		if (touristVouchersCurrent == null) {
			return;
		}

		String val = reader.getText().trim();
		if (val.isEmpty()) {
			return;
		}
		switch (elementName) {
		case "Type":
			touristVouchersCurrent.setType(val.toString());
			break;
		case "Country":
			touristVouchersCurrent.setCountry(val.toString());
			break;
		case "NumberNights":
			touristVouchersCurrent.setNumberNights(Integer.parseInt(val));
			break;
		case "Currency":
			touristVouchersCurrent.setCurrency(val.toString());
			break;
		case "NumberOfAdults":
			touristVouchersCurrent.setNumberOfAdult(Integer.parseInt(val));
			break;
		case "TotalPrice":
			String tmpCost = val.toString();
			Double intCost = Double.parseDouble(tmpCost);
			touristVouchersCurrent.setPrice(intCost);
			break;
		case "StarRating":
			touristVouchersCurrent.setHotelDetails(val.toString());
			break;
		case "BoardBasis":
			String tmpHotelDetails = touristVouchersCurrent.getHotelDetails();
			tmpHotelDetails = tmpHotelDetails + " " + val.toString();
			touristVouchersCurrent.setHotelDetails(tmpHotelDetails);
			break;
		case "RoomTypeDescription":
			tmpHotelDetails = touristVouchersCurrent.getHotelDetails();
			tmpHotelDetails = tmpHotelDetails + " " + val.toString();
			touristVouchersCurrent.setHotelDetails(tmpHotelDetails);
			break;
		case "TypeTransport":
			touristVouchersCurrent.setTransportDetails(val.toString());
		default:
			break;
		}
	}

	private void handleStartElement(XMLStreamReader reader, String elementName) {
		if ("PackageHoliday".equals(elementName)) {
			touristVouchersCurrent = new TouristVoucher();
			touristVouchersCurrent.setId(reader.getAttributeValue(null, "id"));
		}
	}

	@Override
	protected void analize() {
		parse();
	}

	@Override
	public String getName() {
		return NAME;
	}
}
