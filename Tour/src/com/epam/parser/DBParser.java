package com.epam.parser;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.epam.exception.DatabaseException;
import com.epam.model.TouristVoucher;

public class DBParser extends Parser {

	private Connection connection;
	private static final String URL = "jdbc:mysql://localhost:3306/" + "tourist_vouchers";
	private static final String USER = "root";
	private static final String PASSWORD = "root";
	private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
	public static final String NAME = "db";

	private void closeConnection() throws DatabaseException {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new DatabaseException("Connection cannot be created");
		}
	}

	public DBParser() {
		try {
			registerDriver();
			getConnection();

		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void DatabaseParser() throws DatabaseException {

	}

	private void parse() throws DatabaseException {
		Statement st = null;
		ResultSet rs = null;

		try {
			st = getConnection().createStatement();
			rs = st.executeQuery("SELECT * FROM tourist_vouchers_list");
		} catch (SQLException e) {
			throw new DatabaseException("Error during get data from table");
		}

		try {
			while (rs.next()) {
				TouristVoucher touristVouchersCurrent = new TouristVoucher();
				touristVouchersCurrent.setId(rs.getString("id"));
				touristVouchersCurrent.setType(rs.getString("type_tour"));
				touristVouchersCurrent.setCountry(rs.getString("country"));
				touristVouchersCurrent.setNumberNights(rs.getInt("number_night"));
				touristVouchersCurrent.setNumberOfAdult(rs.getInt("number_adult"));
				touristVouchersCurrent.setPrice(rs.getDouble("price"));
				touristVouchersCurrent.setCurrency(rs.getString("currency"));
				touristVouchersCurrent.setHotelDetails(rs.getString("hotel_detail"));
				touristVouchersCurrent.setTransportDetails(rs.getString("transport_detail"));

				list.add(touristVouchersCurrent);
			}
		} catch (SQLException e) {
			throw new DatabaseException(e);
		}

		closeConnection();
	}

	private Connection getConnection() throws DatabaseException {
		try {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			throw new DatabaseException("Connection cannot be created");
		}
		return connection;
	}

	private void registerDriver() throws DatabaseException {
		try {
			Class.forName(DRIVER_NAME);
		} catch (ClassNotFoundException e) {
			throw new DatabaseException("Class for the driver has not been found");
		}
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	protected void analize() {
		try {
			parse();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
	}

}
