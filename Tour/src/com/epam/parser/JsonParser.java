package com.epam.parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.epam.model.TouristVoucher;

public class JsonParser extends Parser {

	private JSONObject jsonObject;
	private JSONParser jsonParser;
	public static final String NAME = "json";

	public JsonParser(String file) {
		jsonParser = new JSONParser();
		try {
			jsonObject = (JSONObject) jsonParser.parse(new FileReader(file));
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void parse()
			throws FileNotFoundException, IOException, ParseException {

		JSONArray jsonTVs = (JSONArray) jsonObject.get("TouristVouchers");

		for (Object jsonTV : jsonTVs) {
			TouristVoucher touristVouchersCurrent = parseTV((JSONObject) jsonTV);
			list.add(touristVouchersCurrent);
		}
	}

	private TouristVoucher parseTV(JSONObject jsonTV) {
		TouristVoucher touristVouchersCurrent = new TouristVoucher();
		JSONObject packageHoliday = (JSONObject) jsonTV.get("PackageHoliday");
		JSONObject cost = (JSONObject) jsonTV.get("Cost");
		JSONObject hotelDetails = (JSONObject) jsonTV.get("HotelDetails");
		JSONObject transportDetails = (JSONObject) jsonTV.get("TransportDetails");
		JSONObject roomDetails = (JSONObject) hotelDetails.get("RoomDetails");

		touristVouchersCurrent.setId((String) packageHoliday.get("id"));
		touristVouchersCurrent.setType((String) packageHoliday.get("type"));
		touristVouchersCurrent.setCountry((String) packageHoliday.get("Country"));
		touristVouchersCurrent.setNumberNights(((Long) packageHoliday.get("NumberNights")).intValue());
		touristVouchersCurrent.setNumberOfAdult(((Long) cost.get("NumberOfAdults")).intValue());
		touristVouchersCurrent.setPrice(((Long) cost.get("TotalPrice")).doubleValue());
		touristVouchersCurrent.setCurrency((String) cost.get("Currency"));

		touristVouchersCurrent.setTransportDetails((String) transportDetails.get("TypeTransport"));
		String hotelDetail = (hotelDetails.get("StarRating")).toString() + " " + roomDetails.get("BoardBasis");
		touristVouchersCurrent.setHotelDetails(hotelDetail);

		return touristVouchersCurrent;

	}

	@Override
	protected void analize() {
		try {
			parse();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getName() {
		return NAME;
	}
}
