package com.epam.parser;

import java.util.ArrayList;
import java.util.List;

import com.epam.model.TouristVoucher;

public abstract class Parser {
	
	protected List<TouristVoucher> list;

	public abstract String getName();

	public final List<TouristVoucher> getTouristVouchers() {
		list = new ArrayList<TouristVoucher>();
		analize();
		return list;
	}

	abstract protected void analize();
}
