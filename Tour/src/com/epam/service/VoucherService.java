package com.epam.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;

import com.epam.controller.VoucherController;
import com.epam.filter.FilterVouscher;
import com.epam.model.TouristVoucher;
import com.epam.parser.DBParser;
import com.epam.parser.JsonParser;
import com.epam.parser.Parser;
import com.epam.parser.XMLParser;

public class VoucherService {

	public List<TouristVoucher> getVouchers(Parser parser, String filterType, String filterCountry) {
		List<TouristVoucher> vouchers = parser.getTouristVouchers();

		sort(vouchers);
		if (filterType !=null && filterCountry != null){
		return filter(filterType, filterCountry, vouchers);
		
		}
		return vouchers;
	}

	private List<TouristVoucher> filter(String filterType, String filterCountry, List<TouristVoucher> vouchers) {
		List<TouristVoucher> filderedVouchers = FilterVouscher.filter(
				vouchers, filterType, filterCountry);
		return filderedVouchers;
	}

	private void sort(List<TouristVoucher> vouchers) {
		Collections.sort(vouchers, new Comparator<TouristVoucher>() {
			@Override
			public int compare(TouristVoucher o1, TouristVoucher o2) {
				// TODO Auto-generated method stub
				return 0;
			}
		});
	}

	public Parser getParser(VoucherController voucherController, String parserParam) throws ServletException {
		Parser parser;
		switch (parserParam) {
		case JsonParser.NAME:
			voucherController.file = voucherController.getServletContext().getRealPath(VoucherController.JSON_FILE);
			parser = new JsonParser(voucherController.file);
			break;
		case XMLParser.NAME:
			voucherController.file = voucherController.getServletContext().getRealPath(VoucherController.XML_FILE);
			parser = new XMLParser(voucherController.file);
			break;
		case DBParser.NAME:
			parser = new DBParser();
			break;
		default:
			throw new ServletException("Incorrect parser parameters");
		}
		return parser;
	}

}
