package com.epam.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.filter.FilterVouscher;
import com.epam.model.TouristVoucher;
import com.epam.parser.Parser;
import com.epam.service.VoucherService;

public class VoucherController extends HttpServlet {

	private static final double PAGE_SIZE = 10.0;
	public static final String XML_FILE = "/WEB-INF/classes/touristvouchers.xml";
	public static final String JSON_FILE = "/WEB-INF/classes/touristvouchers.json";
	
	private VoucherService service = new VoucherService(); 

	public String file;

	@Override
	protected void doGet(
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parserParam = request.getParameter("parser");
		String sortColumn = request.getParameter("sortcolumn");
		String sortType = request.getParameter("sorttype");
		String filterType = request.getParameter("type");
		String filterCountry = request.getParameter("country");

		Integer page = getPage(request);

		Parser parser = service.getParser(this, parserParam);

		List<TouristVoucher> vouchers = service.getVouchers(parser, filterType, filterCountry);
		
		Integer pageCount = ((Double) Math.ceil(vouchers.size() / PAGE_SIZE)).intValue();

		Integer to = (int) (page * PAGE_SIZE);
		Integer from = (int) ((page - 1) * PAGE_SIZE);
		List<TouristVoucher> displayedVouchers = vouchers.subList(
				from, to > vouchers.size() ? vouchers.size() : to);

		request.setAttribute("touristVouchersList", displayedVouchers);
		request.setAttribute("noOfPages", pageCount);
		request.setAttribute("currentPage", page);
		request.setAttribute("parser", parser.getName());
		RequestDispatcher view = request.getRequestDispatcher("displayTouristVouchers.jsp");
		view.forward(request, response);
	}

	private Integer getPage(HttpServletRequest request) {
		Integer page = 1;
		if (request.getParameter("page") != null) {
			page = Integer.parseInt(request.getParameter("page"));
		}
		return page;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}

	@Override
	public void init() throws ServletException {
		super.init();
	}

}
