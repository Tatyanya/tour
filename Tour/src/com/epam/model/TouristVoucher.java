package com.epam.model;

import java.io.Serializable;

public class TouristVoucher implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String type;
	private String country;
	private int numberNights;
	private int numberOfAdult;
	private double price;
	private String currency;
	private String hotelDetails;
	private String transportDetails;

	public TouristVoucher() {

	}

	public TouristVoucher(String id, String type, String country, int numberNights, int numberOfAdult, double price, String currency, String hotelDetails,
			String transportDetails) {
		this.id = id;
		this.type = type;
		this.country = country;
		this.numberNights = numberNights;
		this.numberOfAdult = numberOfAdult;
		this.price = price;
		this.currency = currency;
		this.hotelDetails = hotelDetails;
		this.transportDetails = transportDetails;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getNumberNights() {
		return numberNights;
	}

	public void setNumberNights(int numberNights) {
		this.numberNights = numberNights;
	}
	public double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getHotelDetails() {
		return hotelDetails;
	}

	public void setHotelDetails(String hotelDetails) {
		this.hotelDetails = hotelDetails;
	}

	public String getTransportDetails() {
		return transportDetails;
	}

	public void setTransportDetails(String transportDetails) {
		this.transportDetails = transportDetails;
	}

	public int getNumberOfAdult() {
		return numberOfAdult;
	}

	public void setNumberOfAdult(int numberOfAdult) {
		this.numberOfAdult = numberOfAdult;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((hotelDetails == null) ? 0 : hotelDetails.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + numberNights;
		result = prime * result + numberOfAdult;
		result = (int) (prime * result + price);
		result = prime * result + ((transportDetails == null) ? 0 : transportDetails.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TouristVoucher other = (TouristVoucher) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (hotelDetails == null) {
			if (other.hotelDetails != null)
				return false;
		} else if (!hotelDetails.equals(other.hotelDetails))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numberNights != other.numberNights)
			return false;
		if (transportDetails == null) {
			if (other.transportDetails != null)
				return false;
		} else if (!transportDetails.equals(other.transportDetails))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TouristVouchers [id=" + id + ", type=" + type + ", country=" + country + ", numberNights="
				+ numberNights + ", numberAdults="	+ numberOfAdult +  ", currency=" + currency + ", price=" + price + ", hotelDetails=" + hotelDetails + ", transportDetails="
				+ transportDetails + "]\n";
	}

}
