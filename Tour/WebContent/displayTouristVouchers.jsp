<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tourist Vouchers</title>
</head>
<body>
	<div>
		<h1>The most important heading on this page</h1>

		<form action="/Tour/parsingtest" method="get">
			<input type="hidden" name="parser" value="${parser}"/>
			<table>
				<tr>
					<td>Chose type of holiday</td>
					<td><select name="type">
							<option value="beach">Beach</option>
							<option value="holiday">Holiday</option>
							<option value="health">Health</option>
					</select></td>
				</tr>
				<tr>
					<td>Chose country</td>
					<td><select  name="country">
							<option value="belarus">Belarus</option>
							<option value="spain">Spain</option>
							<option value="greece">Greece</option>
					</select></td>
				</tr>
				<tr>
					<td><input type="submit" value="Perform search"></td>
				</tr>
			</table>
		</form>

	</div>
	<table border="1" cellpadding="5" cellspacing="5">
		<tr>
			<th>TV ID</th>
			<th>Type</th>
			<th>Country</th>
			<th>Number of night</th>
			<th>Number of adults</th>
			<th>Price</th>
			<th>Currency</th>
			<th>Hotel Details</th>
			<th>Transport details</th>
		</tr>

		<c:forEach var="TouristVouchers" items="${touristVouchersList}">
			<tr>
				<td>${TouristVouchers.id}</td>
				<td>${TouristVouchers.type}</td>
				<td>${TouristVouchers.country}</td>
				<td>${TouristVouchers.numberNights}</td>
				<td>${TouristVouchers.numberOfAdult}</td>
				<td>${TouristVouchers.price}</td>
				<td>${TouristVouchers.currency}</td>
				<td>${TouristVouchers.hotelDetails}</td>
				<td>${TouristVouchers.transportDetails}</td>

			</tr>
		</c:forEach>
	</table>

	<%--For displaying Previous link except for the 1st page --%>
	<c:if test="${currentPage != 1}">
		<td><a
			href="/Tour/parsingtest?parser=${parser}&page=${currentPage-1}">Previous</a></td>
	</c:if>

	<%--For displaying Page numbers. 
    The when condition does not display a link for the current page--%>
	<table border="1" cellpadding="5" cellspacing="5">
		<tr>
			<c:forEach begin="1" end="${noOfPages}" var="i">
				<c:choose>
					<c:when test="${currentPage eq i}">
						<td>${i}</td>
					</c:when>
					<c:otherwise>
						<td><a href="/Tour/parsingtest?parser=${parser}&page=${i}">${i}</a></td>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</tr>
	</table>

	<%--For displaying Next link --%>
	<c:if test="${currentPage lt noOfPages}">
		<td><a
			href="/Tour/parsingtest?parser=${parser}&page=${currentPage+1}">Next</a></td>
	</c:if>

	<form method="get">
		<input type="button" value="go back"
			onclick="javascript:history.go(-1)" />
	</form>
</body>
</html>